function testvoxel(fname,resolution,pyramid_levels)
if(nargin<3)
    pyramid_levels=1;
end
fprintf(1, 'Reading in mesh %s.\n', fname); tic;
if strcmp(fname(end-3:end),'cgns')
    [xs, tris]=readcgns_unstr(fname);
    tris=int32(tris);
elseif strcmp(fname(end-2:end),'inp')
    [xs, tris]=readucd_unstr(fname); 
    tris=int32(tris);
elseif strcmp(fname(end-2:end),'ply')
    [xs,tris] = readply (fname);
    tris=int32(tris);
end
fprintf(1, 'Done in %e seconds.\n', toc);
fprintf(1,'%d triangles\n',size(tris,1));
bbox = compute_bbox( xs );
dimx = bbox(4) - bbox(1);
dimy = bbox(5) - bbox(2);
dimz = bbox(6) - bbox(3);
center_surface=0.5*[dimx,dimy,dimz]+[bbox(1),bbox(2),bbox(3)];
for i=pyramid_levels:-1:1
    factor=2^(pyramid_levels-1);
    resolution=resolution/factor;
    resolution=ceil(resolution);
    DimSize=int32(resolution);
    ElementSpacing(1)=dimx/resolution(1);
    ElementSpacing(2)=dimy/resolution(2);
    ElementSpacing(3)=dimz/resolution(3);
    DimSize=DimSize+2;
    center_image=double(0.5*DimSize).*ElementSpacing;
    
    Offset=center_surface-center_image;
    totvox=DimSize(1)*DimSize(2)*DimSize(3);
    fprintf(1,'%d voxels\n',totvox);tic
    voxelated_grid=leanvoxelate(xs,tris,DimSize(1),DimSize(2),DimSize(3),ElementSpacing(1),...
        ElementSpacing(2),ElementSpacing(3),Offset(1),Offset(2),Offset(3));toc
end
fprintf(1,'Writing %s \n',[fname(1:end-5) '_voxelated.mha']);
write_mha([fname(1:end-5) '_voxelated.mha'],uint8(voxelated_grid))
end