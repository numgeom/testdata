function image_test(fname,prefix)
%fname = './Rat_020_reduced_cast.cgns';
debug=true;
[xs, tris] = readcgns_unstr(fname);
nv_clean = int32(size(xs,1)); nt_clean = int32(size(tris,1));
test.type = 'beta';
test.geom = 'closed';
%dir_name = sprintf('%s%s',pwd,'/Cast');
dir_name = sprintf('%s',prefix);
slash = '/';
mkdir(dir_name);
out_dir_name = [dir_name slash];
tris = int32(tris);
niter = int32(30);
angletol_min = 15;
perfolded = 80;
disp_alpha = 1;
check_trank = false;
vc_flag = true;
verbose = int32(2);

test.debug = 0;
test.dirname = out_dir_name;
if debug
    test.debug = 1;
end
    
% Quality measure
test.statistics_all = zeros(19,(niter/5)+1);
[min_angle, max_angle, min_area, max_area] = compute_statistics_tris_global(nt_clean, xs, tris);
test.statistics_all(:,1) = plot_hist_quality(xs,tris(1:nt_clean,:),out_dir_name,'Before_Smoothing');
fprintf('Before smoothing: max angle is %g degree, min angle is %g degree, and area ratio is %g.\n', ...
    max_angle, min_angle, max_area/min_area);

fname = sprintf('initialmesh_%s.vtk',prefix);
data_fname = [out_dir_name fname];
writevtk_unstr(data_fname,xs,tris);
for degree = int32(2)
    % Smoothing
%     [xs, tris, statistics_all] = smooth_mesh_hisurf_imagemesh_debug_try1( nv_clean, nt_clean, xs, tris,...
%     degree, niter, angletol_min, perfolded, disp_alpha, check_trank, vc_flag, verbose, test, statistics_all);
    
   [xs, tris, test] = smooth_mesh_hisurf_imagemesh( nv_clean, nt_clean, xs, tris,...
   degree, niter, angletol_min, perfolded, disp_alpha, check_trank, vc_flag, verbose, test);
    
    % Quality measure
    [min_angle, max_angle, min_area, max_area] = compute_statistics_tris_global(nt_clean, xs, tris);
    fprintf('After smoothing: max angle is %g degree, min angle is %g degree, and area ratio is %g\n', ...
        max_angle, min_angle, max_area/min_area);
    plot_hist_quality_set(test.statistics_all,out_dir_name, 'Comparision_of_quality')
    plot_hist_quality(xs,tris(1:nt_clean,:),out_dir_name,'After_final_iteration');
    
    % Print out optimized mesh
    fname = sprintf('redistmesh_%s_fitting_degree_%d.vtk',prefix,degree);
    data_fname = [out_dir_name fname];
    writevtk_unstr(data_fname,xs,tris);
end



%END FUNCTION
end