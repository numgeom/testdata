      program interpolation

      include 'netcdf.inc'
      integer::msrc,nsrc,mdst,ndst
      integer::num_link_conservative,num_wgt_conservative,num_link_bilinear,&
               num_wgt_bilinear,num_link_bicubic, m_wgt_bicubic
      integer::nwave,mwaves,nmdim,ntime
      parameter (msrc=128,nsrc=64,mdst=360,ndst=180)
      parameter (num_link_conservative=118096,num_wgt_conservative=3)
      parameter (nwave=42,mwaves=43,nmdim=(nwave+1)*(nwave+2)/2)

      double precision, dimension(1:msrc*nsrc)::src_area,src_frac,src_array,&
                                                src_array_grad1,src_array_grad2,src_array_grad3
      double precision, dimension(1:mdst*ndst)::dst_area,dst_frac,dst_array
      integer, dimension(1:num_link_conservative)::src_add1,dst_add1
      double precision, dimension(1:num_wgt_conservative,1:num_link_conservative)::remap_matrix1

      real,dimension(1:msrc,1:nsrc,1)::src
      real,dimension(1:msrc,1:nsrc)::src1
      real,dimension(1:msrc)::xlon
      real,dimension(1:nsrc)::xlat
      real,dimension(1:mdst,1:ndst)::dst
      double precision::s1,s2

      integer,dimension(1:2)::start,scount
      integer,dimension(1:3)::startm,scountm
      integer,dimension(1:1)::starts,scounts
      integer::ncid,rhid   

      real,dimension(1:msrc,1:nsrc)::src_grad1,src_grad2,src_grad3
      integer::i,j,k,irec,nmstrt,nm,m,n 

       real::rm
       complex, dimension(1:(nwave+1),nsrc) :: um,vm
       complex, dimension(1:nmdim) :: rnm
       
!------------------------------------------------------------------------------------------

      real trig((3*msrc/2)+1),aa(nsrc*(msrc+2)),cc(nsrc*(msrc+1))
      double precision workdp(nsrc,2)
      real worksp(4*nsrc*(nsrc+1)+2)
      integer ifax(13),nmodes(nwave+1),indxm(nmdim),indxn(nmdim)
      real   pnm(nmdim,nsrc),hnm(nmdim,nsrc),&
             gwrc(nsrc),gaulats(nsrc),weights(nsrc),del(nmdim),deli(nmdim)

       call sphinit(6.3712e6,msrc,nsrc,mwaves,nmdim,&
                  workdp,worksp, pnm,hnm,gwrc,gaulats,weights,&
                  del,deli,nmodes,indxm,indxn)
       call set99(trig,ifax,msrc)

       open(1,file='scrip-cam-global.dat',form='unformatted',access='direct',recl=msrc*nsrc*4)
       open(2,file='scrip-cam-to-wrf-global.dat',form='unformatted',access='direct',recl=mdst*ndst*4)

!------------------------------------------------------------------------------------------

       starts(1)=1

       startm(1)=1
       startm(2)=1
       startm(3)=1

       scountm(1)=msrc
       scountm(2)=nsrc
       scountm(3)=1

       STATUS=NF_OPEN('cam-global.psfc.nc',NF_NOWRITE,NCID) 
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)

       scounts(1)=nsrc
       STATUS=NF_INQ_VARID(NCID,'lat',RHID)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)
       STATUS=NF_GET_VARA_REAL(NCID,RHID,STARTs,SCOUNTs,xlat)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)

       scounts(1)=msrc
       STATUS=NF_INQ_VARID(NCID,'lon',RHID)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)
       STATUS=NF_GET_VARA_REAL(NCID,RHID,STARTs,SCOUNTs,xlon)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)

       STATUS=NF_INQ_VARID(NCID,'PS',RHID)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)
       STATUS=NF_GET_VARA_REAL(NCID,RHID,STARTM,SCOUNTM,src)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)

       STATUS=NF_CLOSE(NCID) 
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)

!------------------------------------------------------------------------------------------

       start(1)=1
       start(2)=1
       starts(1)=1

       scount(1)=num_wgt_conservative
       scount(2)=num_link_conservative
       scounts(1)=num_link_conservative

       STATUS=NF_OPEN('map_cam_to_wrf_global_conserv.nc',NF_NOWRITE,NCID) 
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)
       STATUS=NF_INQ_VARID(NCID,'remap_matrix',RHID)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)
       STATUS=NF_GET_VARA_DOUBLE(NCID,RHID,START,SCOUNT,remap_matrix1)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)

       STATUS=NF_INQ_VARID(NCID,'src_address',RHID)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)
       STATUS=NF_GET_VARA_INT(NCID,RHID,STARTS,SCOUNTS,src_add1)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)
       STATUS=NF_INQ_VARID(NCID,'dst_address',RHID)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)
       STATUS=NF_GET_VARA_INT(NCID,RHID,STARTS,SCOUNTS,dst_add1)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)

       scounts(1)=msrc*nsrc
       STATUS=NF_INQ_VARID(NCID,'src_grid_area',RHID)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)
       STATUS=NF_GET_VARA_DOUBLE(NCID,RHID,STARTS,SCOUNTS,src_area)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)
       STATUS=NF_INQ_VARID(NCID,'src_grid_frac',RHID)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)
       STATUS=NF_GET_VARA_DOUBLE(NCID,RHID,STARTS,SCOUNTS,src_frac)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)

       scounts(1)=mdst*ndst
       STATUS=NF_INQ_VARID(NCID,'dst_grid_area',RHID)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)
       STATUS=NF_GET_VARA_DOUBLE(NCID,RHID,STARTS,SCOUNTS,dst_area)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)
       STATUS=NF_INQ_VARID(NCID,'dst_grid_frac',RHID)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)
       STATUS=NF_GET_VARA_DOUBLE(NCID,RHID,STARTS,SCOUNTS,dst_frac)
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)
       STATUS=NF_CLOSE(NCID) 
       IF (STATUS .NE. NF_NOERR) CALL HANDLE_ERR(STATUS)

!------------------------------------------------------------------------------------------
      do j=1,nsrc
         do i=1,msrc
            if(xlon(i).gt.180) xlon(i)= xlon(i)-360
         enddo
        enddo  

! lon

      do j=1,nsrc
         do i=1,msrc
            src_array((j-1)*msrc + i)=xlon(i)
            src1(i,j) = src_array((j-1)*msrc + i)
            src_array_grad2((j-1)*msrc + i)= 1/(cos(xlat(j)*3.1415926/180.0)*6371000.0)
            src_array_grad1((j-1)*msrc + i)= 0
         enddo
        enddo   

        dst_array = 0.0
        do n=1,num_link_conservative
            dst_array(dst_add1(n)) = dst_array(dst_add1(n)) + &
                               src_array(src_add1(n))*remap_matrix1(1,n)  + &
                               src_array_grad1(src_add1(n))*remap_matrix1(2,n) + &
                               src_array_grad2(src_add1(n))*remap_matrix1(3,n)
        end do

        do j=1,ndst
         do i=1,mdst
            dst(i,j)=dst_array((j-1)*mdst + i)
         enddo
        enddo
        write(1,rec=1)((src1(i,j),i=1,msrc),j=1,nsrc)
        write(2,rec=1)((dst(i,j),i=1,mdst),j=1,ndst)

        print *,'cam to wrf global conservative conservation 1:'
        print *,'Grid1 Integral = ',sum(src_array*src_area)
        print *,'Grid2 Integral = ',sum(dst_array*dst_area)
        print *,'Difference     = ',sum(src_array*src_area)-sum(dst_array*dst_area)

! lon^2

      do j=1,nsrc
         do i=1,msrc
            src_array((j-1)*msrc + i)=xlon(i)*xlon(i)
            src1(i,j) = src_array((j-1)*msrc + i)
            src_array_grad2((j-1)*msrc + i)= 2*xlon(i)/(cos(xlat(j)*3.1415926/180.0)*6371000.0)
            src_array_grad1((j-1)*msrc + i)= 0
         enddo
        enddo   

        dst_array = 0.0
        do n=1,num_link_conservative
            dst_array(dst_add1(n)) = dst_array(dst_add1(n)) + &
                               src_array(src_add1(n))*remap_matrix1(1,n)  + &
                               src_array_grad1(src_add1(n))*remap_matrix1(2,n) + &
                               src_array_grad2(src_add1(n))*remap_matrix1(3,n)
        end do

        do j=1,ndst
         do i=1,mdst
            dst(i,j)=dst_array((j-1)*mdst + i)
         enddo
        enddo
        write(1,rec=2)((src1(i,j),i=1,msrc),j=1,nsrc)
        write(2,rec=2)((dst(i,j),i=1,mdst),j=1,ndst)

        print *,'cam to wrf global conservative conservation 2:'
        print *,'Grid1 Integral = ',sum(src_array*src_area)
        print *,'Grid2 Integral = ',sum(dst_array*dst_area)
        print *,'Difference     = ',sum(src_array*src_area)-sum(dst_array*dst_area)

! lon^(-6)

      do j=1,nsrc
         do i=1,msrc
            src_array((j-1)*msrc + i)=(xlon(i))**(-6.0)
            src1(i,j) = src_array((j-1)*msrc + i)
            src_array_grad2((j-1)*msrc + i)= -6/(cos(xlat(j)*3.1415926/180.0)*6371000.0)/((xlon(i))**(7.0))
            src_array_grad1((j-1)*msrc + i)= 0
         enddo
        enddo   

        dst_array = 0.0
        do n=1,num_link_conservative
            dst_array(dst_add1(n)) = dst_array(dst_add1(n)) + &
                               src_array(src_add1(n))*remap_matrix1(1,n)  + &
                               src_array_grad1(src_add1(n))*remap_matrix1(2,n) + &
                               src_array_grad2(src_add1(n))*remap_matrix1(3,n)
        end do

        do j=1,ndst
         do i=1,mdst
            dst(i,j)=dst_array((j-1)*mdst + i)
         enddo
        enddo
        write(1,rec=3)((src1(i,j),i=1,msrc),j=1,nsrc)
        write(2,rec=3)((dst(i,j),i=1,mdst),j=1,ndst)

        print *,'cam to wrf global conservative conservation 3:'
        print *,'Grid1 Integral = ',sum(src_array*src_area)
        print *,'Grid2 Integral = ',sum(dst_array*dst_area)
        print *,'Difference     = ',sum(src_array*src_area)-sum(dst_array*dst_area)

! 2 + (cos(lat))^2 * cos(2*lon)

      do j=1,nsrc
         do i=1,msrc
            src_array((j-1)*msrc + i)=2+cos(xlat(j)*3.1415926/180)*cos(xlat(j)*3.1415926/180)*&
            cos(2*xlon(i)*3.1415926/180)
            src1(i,j) = src_array((j-1)*msrc + i)
            src_array_grad2((j-1)*msrc + i)= -2*cos(xlat(j)*3.1415926/180)*sin(2*xlon(i)*3.1415926/180.0)/6371000.0
            src_array_grad1((j-1)*msrc + i)= -2*cos(xlat(j)*3.1415926/180)*sin(xlat(j)*3.1415926/180)*cos(2*xlon(i)*3.1415926/180.0)/6371000.0
         enddo
        enddo   

        dst_array = 0.0
        do n=1,num_link_conservative
            dst_array(dst_add1(n)) = dst_array(dst_add1(n)) + &
                               src_array(src_add1(n))*remap_matrix1(1,n)  + &
                               src_array_grad1(src_add1(n))*remap_matrix1(2,n) + &
                               src_array_grad2(src_add1(n))*remap_matrix1(3,n)
        end do

        do j=1,ndst
         do i=1,mdst
            dst(i,j)=dst_array((j-1)*mdst + i)
         enddo
        enddo
        write(1,rec=4)((src1(i,j),i=1,msrc),j=1,nsrc)
        write(2,rec=4)((dst(i,j),i=1,mdst),j=1,ndst)

        print *,'cam to wrf global conservative conservation 4:'
        print *,'Grid1 Integral = ',sum(src_array*src_area)
        print *,'Grid2 Integral = ',sum(dst_array*dst_area)
        print *,'Difference     = ',sum(src_array*src_area)-sum(dst_array*dst_area)

! 2 + (sin(2*lat))^16 * cos(16*lon)

      do j=1,nsrc
         do i=1,msrc
            src_array((j-1)*msrc + i)=2+(sin(2*xlat(j)*3.1415926/180)**16)*cos(16*xlon(i)*3.1415926/180)
            src1(i,j) = src_array((j-1)*msrc + i)
            src_array_grad2((j-1)*msrc + i)= -16*(sin(2*xlat(j)*3.1415926/180)**16)* &
                                             sin(16*xlon(i)*3.1415926/180.0)/6371000.0/cos(xlat(j)*3.1415926/180)
            src_array_grad1((j-1)*msrc + i)= 32*cos(2*xlat(j)*3.1415926/180)*sin(2*xlat(j)*3.1415926/180)* &
                                             cos(16*xlon(i)*3.1415926/180.0)/6371000.0
         enddo
        enddo   

        dst_array = 0.0
        do n=1,num_link_conservative
            dst_array(dst_add1(n)) = dst_array(dst_add1(n)) + &
                               src_array(src_add1(n))*remap_matrix1(1,n)  + &
                               src_array_grad1(src_add1(n))*remap_matrix1(2,n) + &
                               src_array_grad2(src_add1(n))*remap_matrix1(3,n)
        end do

        do j=1,ndst
         do i=1,mdst
            dst(i,j)=dst_array((j-1)*mdst + i)
         enddo
        enddo
        write(1,rec=5)((src1(i,j),i=1,msrc),j=1,nsrc)
        write(2,rec=5)((dst(i,j),i=1,mdst),j=1,ndst)

        print *,'cam to wrf global conservative conservation 5:'
        print *,'Grid1 Integral = ',sum(src_array*src_area)
        print *,'Grid2 Integral = ',sum(dst_array*dst_area)
        print *,'Difference     = ',sum(src_array*src_area)-sum(dst_array*dst_area)

! psfc         
      do j=1,nsrc
         do i=1,msrc
            src_array((j-1)*msrc + i)=src(i,j,1)
            src1(i,j) = src_array((j-1)*msrc + i)
         enddo
        enddo   

      do nm = 1, nmdim
         rnm(nm) = cmplx(0.0,0.0)
      enddo
 
      call rfft(src1,um,aa,cc,ifax,trig,msrc,nsrc,mwaves,1)

          do 80 j=1,nsrc
          nmstrt = 0
          do 80  m = 1, mwaves
          do 79  n = 1, nmodes(m) 
             nm = nmstrt + n
             rnm(nm)=rnm(nm)+pnm(nm,j)*weights(j)*um(m,j)
79           continue
          nmstrt = nmstrt + nmodes(m)
80        continue   

         do 82 j=1,nsrc
         nmstrt = 0
         do 82 m=1,mwaves
            rm = m-1
            um(m,j) = cmplx(0.,0.)
            vm(m,j) = cmplx(0.,0.)
            do 81 n=1,nmodes(m)
               nm = nmstrt + n
               um(m,j) = um(m,j) + cmplx(0.,rm)*rnm(nm)*pnm(nm,j)
               vm(m,j) = vm(m,j) - rnm(nm)*hnm(nm,j)
81          continue
            nmstrt = nmstrt + nmodes(m)
82   continue

        call rfft(src_grad2,um,aa,cc,ifax,trig,msrc,nsrc,mwaves,-1)
        call rfft(src_grad1,vm,aa,cc,ifax,trig,msrc,nsrc,mwaves,-1) 

        do j=1,nsrc
         do i=1,msrc
            src_array_grad1((j-1)*msrc + i)=src_grad1(i,j)/(cos(xlat(j)*3.1415926/180.0)*6371000.0)
            src_array_grad2((j-1)*msrc + i)=src_grad2(i,j)/(cos(xlat(j)*3.1415926/180.0)*6371000.0)
         enddo
        enddo  

        dst_array = 0.0
        do n=1,num_link_conservative
            dst_array(dst_add1(n)) = dst_array(dst_add1(n)) + &
                               src_array(src_add1(n))*remap_matrix1(1,n)  + &
                               src_array_grad1(src_add1(n))*remap_matrix1(2,n) + &
                               src_array_grad2(src_add1(n))*remap_matrix1(3,n)
        end do

        do j=1,ndst
         do i=1,mdst
            dst(i,j)=dst_array((j-1)*mdst + i)
         enddo
        enddo
        write(1,rec=6)((src(i,j,1),i=1,msrc),j=1,nsrc)
        write(2,rec=6)((dst(i,j),i=1,mdst),j=1,ndst)

        print *,'cam to wrf global conservative conservation 6:'
        print *,'Grid1 Integral = ',sum(src_array*src_area)
        print *,'Grid2 Integral = ',sum(dst_array*dst_area)
        print *,'Difference     = ',sum(src_array*src_area)-sum(dst_array*dst_area)

        close(1)
        close(2)
      end program interpolation

        subroutine handle_err(status)
        integer status
        write(*,*)'error'
        stop 'stopped'
        end
